[TOC]

## About

Workflow automation is critical for Service Desk use cases. 
This is a demo project to showcase the usage of the [`gitlab-triage`](https://gitlab.com/gitlab-org/ruby/gems/gitlab-triage) gem to automatically triage Service Desk issues.

If you are interested in automating your Service Desk workflow, you can follow along this README to learn how to get started. You'll need the following resources:

- [Service Desk used in this demo](https://gitlab.com/gitlab-examples/ops/service-desk-triage/service-desk/-/issues/service_desk)
- [Repository used in this demo](https://gitlab.com/gitlab-examples/ops/service-desk-triage/service-desk/-/tree/main)
- [(Youtube) Walkthrough of this project](https://www.youtube.com/watch?v=4Ti92cuA7-8)

To follow along with our product development in this area:

- [Issue for this demo](https://gitlab.com/gitlab-org/monitor/respond/-/issues/262)
- [Long-term plans for automation](https://gitlab.com/groups/gitlab-org/-/epics/10458)

## How to replicate this setup?

### Have a repository to host your triage code

- Make sure the `Repository` and `CI/CD` options are enabled in your Service Desk project
- The repository will host your triaging rules, and CI/CD will periodically run them

### Add a triage policy file

- Create a file in your repository that will hold your [policy definitions](https://gitlab.com/gitlab-org/ruby/gems/gitlab-triage#defining-a-policy).
- Copy the [policy from this demo](./.triage-policies.yml) or the gem's [example policy](https://gitlab.com/gitlab-org/ruby/gems/gitlab-triage/-/blob/master/support/.triage-policies.example.yml) to that file

### Try it out locally

- Checkout your repository
- Install the gem on your computer and verify it can be run locally
```
gem install gitlab-triage
gitlab-triage --help
```

- Trigger a test dry-run (specifiy your own Service Desk's project ID)
```
gitlab-triage --dry-run --source-id 48004061
```

- You should see a processing summary with a list of resources (e.g. issues) affected and actions to be made (e.g. comments)
- If no resources are found, it might be because your Service Desk issues are confidential by default. In that case:
  - Create a [Project Access Token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) under your Service Desk project (not the Triage project!)
    - Token name: `gitlab-triage` - this will be used as the name for the bot that applies your policies
    - Expiration date: up to you!
    - Role: `Reporter` (this should match [whichever role can see confidential issues](https://docs.gitlab.com/ee/user/project/issues/confidential_issues.html#permissions-and-access-to-confidential-issues)) in your project
    - Scope: `api`
  - If you cannot create a Project Access Token, use a [Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
  - Run the previous command again, but pass it your token via an environment variable
  ```
  gitlab-triage --dry-run --source-id 48004061 --token $GITLAB_API_TOKEN
  ```

### Create a policy: detect feature requests

This policy will find tickets that contain the words "feature request" and apply the ~request::feature label.

In your Service Desk:
- Create a label, name it "request::feature"
- Send an email to your Service Desk, including the words "feature request" in the email body

In your repository:
- Copy the `Detect feature requests and apply ~request::feature label` rule from [`./.triage-policies.yml`](./.triage-policies.yml)  into your own policy file
- Trigger a test dry-run (specifiy your own Service Desk's project ID)
```
gitlab-triage --dry-run --source-id 48004061 --token $GITLAB_API_TOKEN
```
- Notice the Service Desk issue created from your email is picked up by the dry-run:
```
* Total after limiting: 1 resources

The following comments would be posted for the rule **Detect feature requests and apply ~request::feature label**:

.# https://gitlab.com/gitlab-examples/ops/service-desk-triage/service-desk/-/issues/2
\```
/label ~"request::feature"
\```
```
- Trigger a real run (specifiy your own Service Desk's project ID)
```
gitlab-triage --source-id 48004061 --token $GITLAB_API_TOKEN
```

In your Service Desk:
- Open the issue and notice the label has been applied by the `gitlab-triage` bot 🎉
  - The bot will have the same name as your API token

### Create a policy: re-open closed issues with new comments

This policy will re-open closed issues that have since received new comments.

In your Service Desk:
- Create a label, name it "status::reopened"
- Send an email to your Service Desk
- Close the resulting Service Desk issue
- Send a reply to the original email thread, notice a new comment is posted but the issue remains closed

In your repository:
- Copy the `Re-open closed issues that received new activity (e.g. new comment)` rule from [`./.triage-policies.yml`](./.triage-policies.yml).
- Follow the instructions of the previous section to trigger a dry-run and an actual run

In your Service Desk:
- Open the issue and notice it has been reopened, and the label has been applied by the `gitlab-triage` bot 🎉
  - The bot will have the same name as your API token
- Caveat: this rule will re-open closed issues that have any new activity, not just new comments. It's currently not possible to detect new comments only.

### Use CI to run your policies automatically

You can set up a CI pipeline to execute the triage rules automatically at fixed intervals, rather than running them locally.

To do so, follow this example: https://gitlab.com/gitlab-org/ruby/gems/gitlab-triage/-/blob/master/support/.gitlab-ci.example.yml

## Resources to create your own policies

You can take inspiration from `triage-ops` to find more examples of real-world policies used at GitLab:

- Reports: https://about.gitlab.com/handbook/engineering/quality/triage-operations/#triage-reports
- Scheduled automations: https://about.gitlab.com/handbook/engineering/quality/triage-operations/#scheduled-workflow-automation
- All policies: https://gitlab.com/gitlab-org/quality/triage-ops/-/tree/master/policies

Full reference:
- [Conditions](https://gitlab.com/gitlab-org/ruby/gems/gitlab-triage#conditions-field)
- [Actions](https://gitlab.com/gitlab-org/ruby/gems/gitlab-triage#actions-field)
- [Full documentation](https://gitlab.com/gitlab-org/ruby/gems/gitlab-triage/-/blob/master/README.md)

### Important notes

#### Non-Service Desk issues are edited by gitlab-triage by default

If your project contains both Service Desk issues and "normal" issues, the rules you define will be applied to both.

To triage Service Desk issues _only_, add this `ruby` condition to each of your rules:

```yaml
ruby: |
  author == "support-bot" # Ignore non-Service Desk issues
```

If you are using a `ruby` condition already for filtering, add this as a guard:

```yaml
ruby: |
  return unless author == "support-bot" # Ignore non-Service Desk issues
  <...your code here...>
```

See the policies in [`./.triage-policies.yml`](./.triage-policies.yml) for examples.

#### Beware of internal vs public comments

Using the `comment` action on Service Desk issues will lead to your customer receiving that (automated) comment as an email.
- This is good if you want to automate email answers based on triaging
  - e.g. "Thank you for submitting this ticket! We have automatically determined this is a feature request. Please reply if that is incorrect."
- This is bad if you want to post triage comments that should not be sent to your customer
  - e.g. "This ticket has been opened for 30 days, \@team can you please reply to this customer?"

To avoid this, post internal comments using the following in your rule:

```yaml
actions:
  comment: "<...your internal comment here...>"
  comment_internal: true
```

See the `Remind the team to triage issues that haven't been labeled` policy in [`./.triage-policies.yml`](./.triage-policies.yml) for an example.

Note: internal comments require gitlab-triage >= 1.41.0 (https://gitlab.com/gitlab-org/ruby/gems/gitlab-triage/-/releases/v1.41.0).